# Importing necessary libraries
import os
import subprocess
import dbus_next
from keybindings import keys
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal, send_notification
from qtile_extras import widget

# Colors
bg_dim = "1E2326"
bg0 = "272E33"
bg1 = "2E383C"
bg2 = "374145"
bg3 = "414B50"
bg4 = "495156"
bg5 = "4F5B58"
bg_red = "4C3743"
bg_visual = "493B40"
bg_yellow = "45443C"
bg_green = "3C4841"
bg_blue = "384B55"
red = "E67E80"
orange = "E69875"
yellow = "DBBC7F"
green = "A7C080"
blue = "7FBBB3"
aqua = "83C092"
purple = "D699B6"
fg = "D3C6AA"
statusline1 = "A7C080"
statusline2 = "D3C6AA"
statusline3 = "E67E80"
gray0 = "7A8478"
gray1 = "859289"
gray2 = "9DA9A0"


# Variables
mod = "mod4"
terminal = "alacritty"

# Groups
groups = [
    ScratchPad('0', [
        DropDown(
            'helix',
            [terminal, '-e', 'helix'],
            height = 0.965,
            width = 0.49,
            x = 0.5,
            y = 0.015,
            on_focus_lost_hide = False,
            opacity = 1,
            warp_pointer = False,
        ),
        DropDown(
            'lf',
            [terminal, '-e', '/home/vid/.local/bin/lf.sh'],
            height = 0.965,
            width = 0.49,
            x = 0.5,
            y = 0.015,
            on_focus_lost_hide = True,
            opacity = 1,
            warp_pointer = False,
        ),
        DropDown(
            'cmus',
            [terminal, '-e', 'cmus'],
            height = 0.965,
            width = 0.49,
            x = 0.5,
            y = 0.015,
            on_focus_lost_hide = True,
            opacity = 1,
            warp_pointer = False,
        ),
        DropDown(
            'terminal',
            [terminal],
            height = 0.965,
            width = 0.49,
            x = 0.5,
            y = 0.015,
            on_focus_lost_hide = True,
            opacity = 1,
            warp_pointer = False,
        ),
    ]),

    Group('a',
          label=""
    ),

    Group('s',
          label=""
    ),

    Group('d',
          label=""
    ),

    Group('f',
          label=""
    ),

    Group('u',
          label=""
    ),

    Group('i',
          label=""
    ),

    Group('o',
          label=""
    ),

    Group('p',
          label=""
    ),
]

# Keybindings for switching between groups
for i in groups:
    keys.extend([

        Key([mod],i.name,
            lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)
        ),

        Key([mod, "shift"], i.name,
            lazy.window.togroup(i.name),
            desc="Move focused window to group {}".format(i.name)
        ),

    ])

# Layouts
layouts = [
    # Default layout
    layout.Columns(
        border_width=2,
        font="Hurmit Nerd Font",
        margin=3,
        border_focus=green,
        border_normal='494949',
        border_on_single=True,
    ),
    # Secondary layout
    layout.Max(),
]

# Default widget settings
widget_defaults = dict(
    font="Hurmit Nerd Font",
    fontsize=14,
    padding=3,
    foreground=fg
)

extension_defaults = widget_defaults.copy()

# Screens configuration
screens = [
    Screen(
        bottom=bar.Gap(5),
        left=bar.Gap(5),
        right=bar.Gap(5),
        top=bar.Bar(
            [
                # Workspaces
                widget.Spacer(10),
                widget.GroupBox(
                    highlight_method='text',
                    rounded=False,
                    disable_drag=True,
                    active="#979797",
                    inactive="#494949",
                    this_current_screen_border=green,
                    margin_y=2,
                    margin_x=0,
                    padding_y=0,
                    fontsize=20,
                    font="Hurmit Nerd Font",
                ),
                widget.Spacer(10),

                # System tray
                widget.Systray(),
                widget.Spacer(),

                # Clock
                widget.Clock(
                    format=" %H:%M  %b %d",
                ),
                widget.Spacer(),

                # Connection info
                widget.Wlan(
                    format='  {essid}',
                ),
                widget.Spacer(10),

                # Volume
                widget.Volume(
                    fmt='󰕾 {}',
                ),
                widget.Spacer(10),

                # Battery (install upower)
                widget.Spacer(10),
                widget.UPowerWidget(
                    border_colour=fg,
                    border_critical_colour=red,
                    border_charge_colour=fg,
                    fill_normal=fg,
                    fill_critical=fg,
                    fill_low=fg,
                    text_charging='({percentage:.0f}%)󰚥 ',
                    text_discharging='{percentage:.0f}% ',
                ),
                widget.Spacer(10),
            ],
            36,
            background=bg0,
            margin=[0, 0, 5, 0],
        ),
    ),
]

# Drag floating layouts
mouse = [
    Drag([mod], "Button1",
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),

    Drag([mod], "Button3",
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),

    Click([mod], "Button2",
          lazy.window.bring_to_front()),
]

# Floating layout rules
floating_layout = layout.Floating(
    font="Hurmit Nerd Font",
    border_focus=green,
    border_normal='494949',
    border_width=2,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(wm_class="feh"), # image viewer
        Match(wm_class="qvidcap"), # QT video capture
        Match(wm_class="blueman-manager"),
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)

# Other configurations
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wmname = "qtile"
cursor_warp = True
follow_mouse_focus = False
bring_front_click = False

# Autostart
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([home])
