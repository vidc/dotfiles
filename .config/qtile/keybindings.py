
#    ██╗███╗   ███╗██████╗  ██████╗ ██████╗ ████████╗███████╗
#    ██║████╗ ████║██╔══██╗██╔═══██╗██╔══██╗╚══██╔══╝██╔════╝
#    ██║██╔████╔██║██████╔╝██║   ██║██████╔╝   ██║   ███████╗
#    ██║██║╚██╔╝██║██╔═══╝ ██║   ██║██╔══██╗   ██║   ╚════██║
#    ██║██║ ╚═╝ ██║██║     ╚██████╔╝██║  ██║   ██║   ███████║
#    ╚═╝╚═╝     ╚═╝╚═╝      ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝

import os
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = "alacritty"
browser = "firefox"



#    ███╗   ███╗ ██████╗ ██╗   ██╗███████╗    ██╗    ██╗██╗███╗   ██╗██████╗  ██████╗ ██╗    ██╗███████╗
#    ████╗ ████║██╔═══██╗██║   ██║██╔════╝    ██║    ██║██║████╗  ██║██╔══██╗██╔═══██╗██║    ██║██╔════╝
#    ██╔████╔██║██║   ██║██║   ██║█████╗      ██║ █╗ ██║██║██╔██╗ ██║██║  ██║██║   ██║██║ █╗ ██║███████╗
#    ██║╚██╔╝██║██║   ██║╚██╗ ██╔╝██╔══╝      ██║███╗██║██║██║╚██╗██║██║  ██║██║   ██║██║███╗██║╚════██║
#    ██║ ╚═╝ ██║╚██████╔╝ ╚████╔╝ ███████╗    ╚███╔███╔╝██║██║ ╚████║██████╔╝╚██████╔╝╚███╔███╔╝███████║
#    ╚═╝     ╚═╝ ╚═════╝   ╚═══╝  ╚══════╝     ╚══╝╚══╝ ╚═╝╚═╝  ╚═══╝╚═════╝  ╚═════╝  ╚══╝╚══╝ ╚══════╝

def move_h(qtile):
    if qtile.current_window.floating:
        qtile.current_window.move_floating(-50, 0),
    else:
        qtile.current_layout.shuffle_left(),

def move_j(qtile):
    if qtile.current_window.floating:
        qtile.current_window.move_floating(0, 50),
    else:
        qtile.current_layout.shuffle_down(),

def move_k(qtile):
    if qtile.current_window.floating:
        qtile.current_window.move_floating(0, -50),
    else:
        qtile.current_layout.shuffle_up(),

def move_l(qtile):
    if qtile.current_window.floating:
        qtile.current_window.move_floating(50, 0),
    else:
        qtile.current_layout.shuffle_right(),

# resize floating and tiled windows
def resize_h(qtile):
    if qtile.current_window.floating:
        qtile.current_window.resize_floating(-50, 0),
    else:
        qtile.current_layout.grow_left(),

def resize_j(qtile):
    if qtile.current_window.floating:
        qtile.current_window.resize_floating(0, 50),
    else:
        qtile.current_layout.grow_down(),

def resize_k(qtile):
    if qtile.current_window.floating:
        qtile.current_window.resize_floating(0, -50),
    else:
        qtile.current_layout.grow_up(),

def resize_l(qtile):
    if qtile.current_window.floating:
        qtile.current_window.resize_floating(50, 0),
    else:
        qtile.current_layout.grow_right(),

def normalize(qtile):
    if qtile.current_window.floating:
        qtile.current_window.set_size_floating(500, 500),
        qtile.current_window.center(),
    else:
        qtile.current_layout.normalize(),


#    ██╗  ██╗███████╗██╗   ██╗██████╗ ██╗███╗   ██╗██████╗ ██╗███╗   ██╗ ██████╗ ███████╗
#    ██║ ██╔╝██╔════╝╚██╗ ██╔╝██╔══██╗██║████╗  ██║██╔══██╗██║████╗  ██║██╔════╝ ██╔════╝
#    █████╔╝ █████╗   ╚████╔╝ ██████╔╝██║██╔██╗ ██║██║  ██║██║██╔██╗ ██║██║  ███╗███████╗
#    ██╔═██╗ ██╔══╝    ╚██╔╝  ██╔══██╗██║██║╚██╗██║██║  ██║██║██║╚██╗██║██║   ██║╚════██║
#    ██║  ██╗███████╗   ██║   ██████╔╝██║██║ ╚████║██████╔╝██║██║ ╚████║╚██████╔╝███████║
#    ╚═╝  ╚═╝╚══════╝   ╚═╝   ╚═════╝ ╚═╝╚═╝  ╚═══╝╚═════╝ ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝

keys = [

    # Rofi
    Key([mod], "m",
        lazy.spawn(os.path.expanduser("rofi -show drun")),
        desc="rofi"
    ),

    Key([mod, "control"], "m",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/powermenu")),
        desc="rofi power menu"
    ),

    Key([mod, "shift"], "m",
        lazy.spawn(os.path.expanduser("rofi -show calc")),
        desc="rofi calc"
    ),

    Key(["mod1"], "Tab",
        lazy.spawn(os.path.expanduser("rofi -show window")),
        desc="rofi window"
    ),

    Key([mod], "c",
        lazy.spawn(os.path.expanduser("rofi -modi 'clipboard:greenclip print' -show clipboard -run-command '{cmd}' -config ~/.config/rofi/greenclip.rasi ")),
        desc="Greenclip clipboard manager."
    ),

    # Player
    Key([mod], "down",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/playerctl.sh toggle")),
        desc="play/pause"
    ),

    Key([mod], "up",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/playerctl.sh shuffle")),
        desc="shuffle"
    ),

    Key([mod], "right",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/playerctl.sh next")),
        desc="next"
    ),

    Key([mod], "left",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/playerctl.sh prev")),
        desc="prev"
    ),
    
    # Volume
    Key([], "XF86AudioMicMute",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/volume.sh mic")),
        desc="mute/unmute mic"
    ),

    Key([], "XF86AudioRaiseVolume",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/volume.sh up")),
        desc="vol up"
    ),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/volume.sh down")),
        desc="vol down"
    ),

    Key([], "XF86AudioMute",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/volume.sh mute")),
        desc="mute/unmute"
    ),

  
  # Brightness

    Key([], "XF86MonBrightnessUp",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/brightness.sh up")),
        desc="Brightness up"
    ),

    Key([], "XF86MonBrightnessDown",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/brightness.sh down")),
        desc="Brightness down"
    ),

     
  # Scrot

    Key([], "Print",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/screenshot.sh ")),
        desc="normal screenshot"
    ),

    Key(["shift"], "Print",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/screenshot.sh select")),
        desc="selection screenshot"
    ),

    Key(["control"], "Print",
        lazy.spawn(os.path.expanduser("~/.config/qtile/scripts/screenshot.sh window")),
        desc="window screenshot"
    ),

    # Camera
    Key([], "XF86Display",
        lazy.spawn(os.path.expanduser("~/./.local/bin/display.sh")),
        desc="Toggle display"
    ),

    # Switch between windows
    Key([mod], "h",
        lazy.layout.left(),
        desc="Move focus to left"
    ),

    Key([mod], "l",
        lazy.layout.right(),
        desc="Move focus to right"
    ),

    Key([mod], "j",
        lazy.layout.down(),
        desc="Move focus down"
    ),

    Key([mod], "k",
        lazy.layout.up(),
        desc="Move focus up"
    ),

    Key([mod], "space",
        lazy.group.next_window(),
        desc="Move window focus to other window"
    ),

    Key([mod, "shift"], "space",
        lazy.group.prev_window(),
        desc="Move window focus to other window"
    ),


    # Grow windows
    Key([mod, "shift"], "h",
        lazy.function(move_h),
        desc="Move window to the left",
    ),

    Key([mod, "shift"], "j",
        lazy.function(move_j),
        desc="Move window down",
    ),

    Key([mod, "shift"], "k",
        lazy.function(move_k),
        desc="Move window up",
    ),

    Key([mod, "shift"], "l",
        lazy.function(move_l),
        desc="Move window to the right",
    ),

    # resize window
    Key([mod, "control"], "h",
        lazy.function(resize_h),
        desc="Grow window to the left",
    ),

    Key([mod, "control"], "j",
        lazy.function(resize_j),
        desc="Grow window down",
    ),

    Key([mod, "control"], "k",
        lazy.function(resize_k),
        desc="Grow window up",
    ),

    Key([mod, "control"], "l",
        lazy.function(resize_l),
        desc="Grow window to the right",
    ),

    Key([mod], "n",
        lazy.function(normalize),
        desc="Reset all window sizes or center floating",
    ),

    # Windows
    Key([mod], "Return",
        lazy.spawn(terminal),
        desc="Launch terminal"
    ),

    Key([mod], "w",
        lazy.spawn(browser),
        desc="Launch browser"
    ),

    Key([mod], "q",
        lazy.window.kill(),
        desc="Kill focused window"
    ),

    Key([mod], "n",
        lazy.layout.normalize(),
        desc="Reset all window sizes"
    ),

    
    # qtile

    Key([mod, "control"], "r",
        lazy.restart(),
        desc="Reload the config"
    ),

    Key([mod, "control","shift"], "q",
        lazy.shutdown(),
        desc="Shutdown Qtile"
    ),

    # Layouts
    Key([mod], "1",
        lazy.window.toggle_floating(),
        desc="Toggle floating layout"
    ),

    Key([mod], "2",
        lazy.window.toggle_fullscreen(),
        desc="Toggle fullscreen layout"
    ),

    Key([mod], "3",
        lazy.window.toggle_minimize(),
        desc="Toggle minimization on focused window"
    ),

    Key([mod], "4",
        lazy.window.bring_to_front(),
        desc="bring the selected window to the front"
    ),

    Key([mod], "Tab",
        lazy.next_layout(),
        desc="Toggle between layouts"
    ),

    Key([mod], 'b',
        lazy.group['0'].dropdown_toggle('helix'),
        lazy.window.bring_to_front()
    ),

    Key([mod], 'v',
        lazy.group['0'].dropdown_toggle('lf'),
        lazy.window.bring_to_front()
    ),

    Key([mod], 'g',
        lazy.group['0'].dropdown_toggle('cmus'),
        lazy.window.bring_to_front()
    ),

    Key([mod, "control"], "Return",
        lazy.group['0'].dropdown_toggle('terminal'),
        lazy.window.bring_to_front()
    ),
]
