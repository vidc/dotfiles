#!/bin/bash
set -eu
# You can call this script like this:
# $./playerctl.sh toggle
# $./playerctl.sh shuffle
# $./playerctl.sh next
# $./playerctl.sh prev

# Icons
pause=~/.config/qtile/icons/pause.png
play=~/.config/qtile/icons/play.png
shuffle=~/.config/qtile/icons/shuffle.png
next=~/.config/qtile/icons/next.png
prev=~/.config/qtile/icons/prev.png

status=$(playerctl status)

notify () {
    if [ "$status" = Playing ]; then
        dunstify -i "$pause" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "$(playerctl metadata title)"
    else
        dunstify -i "$play" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "$(playerctl metadata title)"
    fi
}
shuffle_status=$(playerctl shuffle)

shuffle_toggle () {
    if [ "$shuffle_status" = On ]; then
        dunstify -i "$shuffle" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "Shuffle Off."
    else
        dunstify -i "$shuffle" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "Shuffle On."
    fi
}

case $1 in
    toggle)
    playerctl play-pause
    notify
    ;;
    next)
    playerctl next
    dunstify -i "$next" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "Next"
    # sleep 1.5
    # dunstify -i "$play" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "$(playerctl metadata title)"
    ;;
    prev)
    playerctl previous
    dunstify -i "$prev" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "Previous"
    # sleep 1.5
    # dunstify -i "$play" -t 1600 -h string:x-dunst-stack-tag:playerctl -u normal "$(playerctl metadata title)"
    ;;
    shuffle)
    playerctl shuffle toggle
    shuffle_toggle
    ;;
esac
