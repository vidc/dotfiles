#!/bin/bash
echo "Do you really want to trash this item?"
file=$(basename "$1")
read ans
if [ "$ans" = "yes" ] || [ "$ans" = "y" ] || [ "$ans" = "YES" ] || [ "$ans" = "Y" ]; then
    ~/./.config/lf/scripts/move.sh "$file" ~/trash
fi
