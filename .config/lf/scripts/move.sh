#!/bin/bash

source_file="$1"
destination_directory="$2"
destination_file="$2/$1"

# Check if file already exists
if [ -e "$destination_file" ]; then
    # Find a unique filename by increasing the counter
    counter=1
    while [ -e "$destination_file~$counter" ]; do
        ((counter++))
    done
    mv "$source_file" "$destination_file~$counter"
else
    mv "$source_file" "$destination_file"
fi
