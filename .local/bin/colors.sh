#!/bin/bash
set -eu

# Colors
bg_dim="1E2326"
bg0="272E33"
bg1="2E383C"
bg2="374145"
bg3="414B50"
bg4="495156"
bg5="4F5B58"
bg_red="4C3743"
bg_visual="493B40"
bg_yellow="45443C"
bg_green="3C4841"
bg_blue="384B55"
red="E67E80"
orange="E69875"
yellow="DBBC7F"
green="A7C080"
blue="7FBBB3"
aqua="83C092"
purple="D699B6"
fg="D3C6AA"
statusline1="A7C080"
statusline2="D3C6AA"
statusline3="E67E80"
gray0="7A8478"
gray1="859289"
gray2="9DA9A0"

case $1 in
    bg_dim)echo "$bg_dim" | xclip -rmlastnl -selection clipboard;;
    bg0)echo "$bg0" | xclip -rmlastnl -selection clipboard;;
    bg1)echo "$bg1" | xclip -rmlastnl -selection clipboard;;
    bg2)echo "$bg2" | xclip -rmlastnl -selection clipboard;;
    bg3)echo "$bg3" | xclip -rmlastnl -selection clipboard;;
    bg4)echo "$bg4" | xclip -rmlastnl -selection clipboard;;
    bg5)echo "$bg5" | xclip -rmlastnl -selection clipboard;;
    bg_red)echo "$bg_red" | xclip -rmlastnl -selection clipboard;;
    bg_visual)echo "$bg_visual" | xclip -rmlastnl -selection clipboard;;
    bg_yellow)echo "$bg_yellow" | xclip -rmlastnl -selection clipboard;;
    bg_green)echo "$bg_green" | xclip -rmlastnl -selection clipboard;;
    bg_blue)echo "$bg_blue" | xclip -rmlastnl -selection clipboard;;
    red)echo "$red" | xclip -rmlastnl -selection clipboard;;
    orange)echo "$orange" | xclip -rmlastnl -selection clipboard;;
    yellow)echo "$yellow" | xclip -rmlastnl -selection clipboard;;
    green)echo "$green" | xclip -rmlastnl -selection clipboard;;
    blue)echo "$blue" | xclip -rmlastnl -selection clipboard;;
    aqua)echo "$aqua" | xclip -rmlastnl -selection clipboard;;
    purple)echo "$purple" | xclip -rmlastnl -selection clipboard;;
    fg)echo "$fg" | xclip -rmlastnl -selection clipboard;;
    statusline1)echo "$statusline1" | xclip -rmlastnl -selection clipboard;;
    statusline2)echo "$statusline2" | xclip -rmlastnl -selection clipboard;;
    statusline3)echo "$statusline3" | xclip -rmlastnl -selection clipboard;;
    gray0)echo "$gray0" | xclip -rmlastnl -selection clipboard;;
    gray1)echo "$gray1" | xclip -rmlastnl -selection clipboard;;
    gray2)echo "$gray2" | xclip -rmlastnl -selection clipboard;;
esac
    

